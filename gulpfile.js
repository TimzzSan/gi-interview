'use strict';

//require
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify-es').default,
    gulpIf = require('gulp-if'),
    cssnano = require('cssnano'),
    sourcemaps = require('gulp-sourcemaps'),
    useref = require('gulp-useref'),
    cssbeautify = require('gulp-cssbeautify'),
    browserSync = require('browser-sync'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    postcss = require('gulp-postcss'),
    atImport = require('postcss-import'),
    cssnext = require('postcss-cssnext'),
    browserReport = require('postcss-browser-reporter'),
    rtlcss = require('gulp-rtlcss'),
    rename = require('gulp-rename'),
    wait = require('gulp-wait');

//paths
var paths = {
    sass: {
        src: './src/sass',
        files: './src/sass/**/*.scss',
        dest: './src/css'
    },
    js: {
        src: './src/js',
        files: './src/js/*.js',
        dest: './dist/js'
    },
    sync: {
        css: 'src/css/**/*.css',
        html: 'src/**/*.html',
        js: 'src/js/**/*.js'
    },
    img: {
        src: './src/img/**/*.!(db)',
        dest: './dist/img'
    },
    fonts: {
        src: './src/fonts/**/*',
        dest: './dist/fonts'
    },
    assets: {
        src: './src/assets/**/*',
        dest: './dist/assets'
    }
}

//browser sync
var server = browserSync.create();

function reload(done) {
    server.reload();
    done();
};

function serve(done) {
    server.init({
        port: 3014,
        server: "./",
        startPath: "/src"
    });
    done();
};

//compile sass
gulp.task('main-sass', function() {
    var plugins = [
        atImport(),
        cssnext({
            browsers: ['last 10 versions', '> 1%', 'Firefox ESR']
        }),
        //browserReport()
    ];
    return gulp.src(paths.sass.files)
        .pipe(sourcemaps.init({largeFile: true}))
        .pipe(wait(200))
        .pipe(sass({
            outputStyle: 'expanded',
            includePaths: [paths.sass.src]
        }).on('error', sass.logError))
        .pipe(postcss(
            plugins
        ))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest(paths.sass.dest));
});

gulp.task('rtl-sass', function() {
    var plugins = [
        atImport(),
        cssnext({
            browsers: ['last 10 versions', '> 1%', 'Firefox ESR']
        }),
        //browserReport()
    ];
    return gulp.src(paths.sass.files)
        .pipe(sourcemaps.init({largeFile: true}))
        .pipe(sass({
            outputStyle: 'expanded',
            includePaths: [paths.sass.src]
        }).on('error', sass.logError))
        .pipe(postcss(
            plugins
        ))
        .pipe(rtlcss({
                stringMap: [{
                    name: 'previous-next',
                    priority: 100,
                    search: ['previous', 'Previous', 'PREVIOUS'],
                    replace: ['next', 'Next', 'NEXT'],
                    options: {
                        scope: '*',
                        ignoreCase: false
                    }
                }]
            }))
        .pipe(rename({ suffix: '-rtl' }))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest(paths.sass.dest));
});

gulp.task('sass', gulp.parallel('main-sass', 'rtl-sass'));

//watcher
function watch(done) {
    gulp.watch(paths.sass.files).on('change', gulp.series('sass', reload));
    gulp.watch(paths.sync.html).on('change', gulp.series(reload));
    gulp.watch(paths.sync.js).on('change', gulp.series(reload));
    done();
};
//gulp.task('watch', gulp.series('sass', serve, watch));
gulp.task('watch', gulp.series('main-sass', serve, watch));

//compress
gulp.task('useref', function () { 
    var plugins = [
        cssnext({
            browsers: ['last 10 versions', '> 1%', 'Firefox ESR']
        }),
        cssnano({
            autoprefixer: false,
            safe: true,
            discardComments: {
                removeAll: true
            }
        })
    ];
    return gulp.src(paths.sync.html)
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify()))
        .pipe(gulpIf('*.css', postcss(
            plugins
        )))
        .pipe(gulp.dest('dist'));
});

//copy fonts
gulp.task('fonts', function () {
    return gulp.src(paths.fonts.src)
        .pipe(gulp.dest(paths.fonts.dest))
});

//copy assets
gulp.task('assets', function () {
    return gulp.src(paths.assets.src)
        .pipe(gulp.dest(paths.assets.dest))
});

//copy images
gulp.task('images', function () {
    return gulp.src(paths.img.src)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(paths.img.dest))
});

//build
gulp.task('default', gulp.series(gulp.parallel('sass', 'images', 'fonts', 'assets'), 'useref'));